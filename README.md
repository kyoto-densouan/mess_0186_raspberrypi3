#mess ver.0186 for RaspberryPi3B+#

##はじめに##
ここに記載した以上のサポートはしません。

ROM・ファイルイメージについての問い合わせの対応はしません。

性能・動作は満足できるものではありません。コードに変更は加えていないため「この程度のもの」と思てください。


##概要##
RaspberryPi3B+でビルドしたmess ver.0186です。

「とりあえず起動できる」oldPCエミュレータとして使用します。

性能、使いやすさ等の期待すべきエミュレータがリリースされるまでのツナギです。

要点のみ記載します。

##使用環境##
RaspberryPi3B+で使用します。（A+等ではほぼ実用になりません）

16GByte以上のmicroSDを使用する。（8GByteだと不足気味です）

ビルドしなければRspbian標準設定のスワップで足りると思います。

ディスプレイ、キーボード、ジョイスティック等も必要に応じて用意してください。

##エミュレータで使用するROM、ディスクイメージについて##
必要に応じて各自用意してください。

##Raspbianの用意##
以下をmicroSDにインストールした環境で動作を確認しています。

----

https://www.raspberrypi.org/downloads/raspbian/

RASPBIAN JESSIE WITH DESKTOP

Image with desktop based on Debian Jessie

Version:June 2017

Release date:2017-06-21

Kernel version:4.9

----

インストールは他のブログ等参照してください。

##環境の準備##

###クロックアップ###
標準設定では性能が足りません。

root権限で/boot/config.txtを編集し、以下の行を追加してください。

----

arm_freq=1200

----

###apt-getの変更###
root権限で/etc/apt/sources.listを編集し以下のように変更します。

----

deb http://mirrordirector.raspbian.org/raspbian/ jessie main contrib non-free rpi

----

↓

----

deb http://mirrordirector.raspbian.org/raspbian/ stretch main contrib non-free rpi

----

apt-getを更新します。

----

sudo apt-get update

----

###必要なライブラリ等のインストール###
以下を実行します。

----

sudo apt-get install gcc-5 g++-5

sudo apt-get install libsdl2-dev libsdl2-ttf-dev 

sudo apt-get install qtcreator

----

qtcreatorのインストールで途中に確認があり止まります。

・qで進む

・yesで進む

で進めてください。

##messの展開##
以下のファイルをダウンロードしてRaspberrypiのmicroSD内においてください。

わかりやすいのはユーザのホームに置くことでしょう。

mess_0186_raspberrypi3_20170701.tar.gz

コマンドラインでファイルがあるディレクトリで以下を実行してください。

----

tar -zxvf mess_0186_raspberrypi3_20170701.tar.gz

----

ファイルが展開され、実行したディレクトリに「mess」ディレクトリが作成されます。

##必要なファイルの配置##
messディレクトリ内のromsディレクトリに必要なROMファイルとディスク・カセットテープイメージを配置してください。

x1の場合は以下のようになります。

----

~/mess

~/mess/mess

~/mess/roms

~/mess/roms/x1.zip

~/mess/roms/x1turbo.zip

~/mess/roms/x1turbo40.zip

~/mess/roms/x1_cass/****.zip

~/mess/roms/x1_flop/****.zip

----

2Dファイル、D88ファイルもromsディレクトリに置いたほうがよいでしょう。

##messの起動##
messディレクトリ内でコマンドラインから以下を実行してください。

----

./mess [起動PC名] [起動イメージ]

----

機種名はx1、x1turbo等になります。

ゼビウスを起動する場合以下のようになります。

----

./mess x1 xevious

----

2Dファイル、D88ファイルを直接指定する場合は以下のようになります。

----

./mess x1turbo -flop1 roms/spaceharrier1.d88 -flop2 roms/spaceharrier2.d88

----

##messの終了##
特殊キーで操作モードを変更する必要があります。

「alt」＋「Win」キーでpartialモードに切り替えて、「esc」で終了します。

詳細は他のブログ等を参照してください。

##動作イメージ##
https://www.youtube.com/watch?v=4XNm7aVcLd8&t=13s

https://www.youtube.com/watch?v=dKg9hLZMUyk


以上。